<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Butaca extends Model
{
    //
    protected $table = 'butaca';
    protected $fillable = ['fila','columna'];
    protected $hidden = ['reservado'];
}
