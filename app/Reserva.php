<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    //
    protected $table = 'reserva';

    public function usuario(){
        return $this->hasMany('App\Usuario');
    }
    public function butaca(){
        return $this->hasMany('App\Butaca');
    }
}
