<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Butaca;
class ButacaCotroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $butacas=Butaca::orderBy('id', 'ASC');
        return view('butaca.index', compact('butacas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('butaca.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->reservado = false;
        $this->validate($request,[ 'fila'=>'required', 'columna'=>'required']);
        Butaca::create($request->all());
        return redirect()->route('butaca.index')->with('success','Registro creado satisfactoriamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $butaca=Butaca::find($id);
        return  view('butaca.show',compact('butaca'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $butaca=Butaca::find($id);
        return view('butaca.edit',compact('butaca'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[ 'fila'=>'required', 'columna'=>'required']);

        butaca::find($id)->update($request->all());
        return redirect()->route('butaca.index')->with('success','Registro actualizado satisfactoriamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Butaca::find($id)->delete();
        return redirect()->route('butaca.index')->with('success','Registro eliminado satisfactoriamente');
    }
}
