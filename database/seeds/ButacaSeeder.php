<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ButacaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create();
        for ($i=1; $i < 6; $i++) {
            for ($j=1; $j < 11; $j++){

                \DB::table('butaca')->insert(array(
                    'fila' => $i,
                    'columna'  => $j,
                    'reservado' => false,
                    'created_at' => date('Y-m-d H:m:s'),
                    'updated_at' => date('Y-m-d H:m:s')
                ));
            }
        }
    }
}
