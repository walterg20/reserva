@extends('layout')
@section('content')
<div class="row">
  <section class="content">
    <div class="col-lg-12 col-lg-offset-2">
      <div class="card">
        <div class="card-header">
            <div class="pull-left"><h3>Lista Butacas</h3></div>
            <div class="pull-right">
              <div class="btn-group">
                <a href="{{ route('butaca.create') }}" class="btn btn-info" >Añadir Butaca</a>
              </div>
            </div>
        </div>
        <div class="card-body">
          <div class="table-container">
            <table id="mytable" class="table table-bordred table-striped">
             <thead>
               <th>Fila</th>
               <th>Columna</th>
               <th>Editar</th>
               <th>Eliminar</th>
             </thead>
             <tbody>
              @if($butacas->count())  
              @foreach($butacas as $butaca)  
              <tr>
                <td>{{$butaca->fila}}</td>
                <td>{{$butaca->columna}}</td>
                <td><a class="btn btn-primary btn-lg" href="{{action('ButacaCotroller@edit', $butaca->id)}}" ><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td>
                  <form action="{{action('ButacaCotroller@destroy', $butaca->id)}}" method="post">
                   {{csrf_field()}}
                   <input name="_method" type="hidden" value="DELETE">

                   <button class="btn btn-danger btn-lg" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                 </td>
               </tr>
               @endforeach 
               @else
               <tr>
                <td colspan="8"> No hay registro !!</td>
              </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection