@extends('layout')
@section('content')
<div class="row">
  <section class="content">
    <div class="col-lg-12 col-lg-offset-2">
      <div class="card">
        <div class="card-header">
            <div class="pull-left"><h3>Lista Butacas</h3></div>
            <div class="pull-right">
              <div class="btn-group">
                <a class="btn btn-info" >Butacas Libre</a>
                <a class="btn btn-danger" >Butacas Ocupadas</a>
              </div>
            </div>
        </div>
        <div class="card-body">
          <div class="table-container">
            <table id="mytable" class="table table-bordred table-striped">
              @if($butacas->count())  
             {{$inx = 1}}
              @foreach($butacas as $butaca)
               
                    @if($inx < 11)
                      <td> <button class="btn {{$butaca->reservado? 'btn-danger': 'btn-info' }}">Butaca{{$butaca->fila." ". $inx}} 
                      
                      </td>  {{$inx++}}
                    @else 
                    {{$inx=1}};
                    <tr>
                    @endif
               @endforeach 
               @else
               <tr>
                <td colspan="8"> No hay registro !!</td>
              </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection