@extends('layout')
@section('content')
<div class="row">
  <section class="content">
    <div class="col-lg-12 col-lg-offset-2">
      <div class="card">
        <div class="card-header">
            <div class="pull-left"><h3>Lista Usuarios</h3></div>
            <div class="pull-right">
              <div class="btn-group">
                <a href="{{ route('usuario.create') }}" class="btn btn-info" >Añadir Usuario</a>
              </div>
            </div>
        </div>
        <div class="card-body">
          <div class="table-container">
            <table id="mytable" class="table table-bordred table-striped">
             <thead>
               <th>Nombre</th>
               <th>Apellido</th>
               <th>Editar</th>
               <th>Eliminar</th>
             </thead>
             <tbody>
              @if($usuarios->count())  
              @foreach($usuarios as $usuario)  
              <tr>
                <td>{{$usuario->nombre}}</td>
                <td>{{$usuario->apellido}}</td>
                <td><a class="btn btn-primary btn-lg" href="{{action('UsuarioCotroller@edit', $usuario->id)}}" ><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td>
                  <form action="{{action('UsuarioCotroller@destroy', $usuario->id)}}" method="post">
                   {{csrf_field()}}
                   <input name="_method" type="hidden" value="DELETE">

                   <button class="btn btn-danger btn-lg" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                 </td>
               </tr>
               @endforeach 
               @else
               <tr>
                <td colspan="8"> No hay registro !!</td>
              </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection